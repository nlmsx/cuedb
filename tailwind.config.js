module.exports = {
  purge: [],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      colors: {
        "cue1": "#FFFCF2",
        "cue2": "#CCC5B9",
        "cue3": "#403D39",
        "cue4": "#252422",
        "cue5": "#EB5E28"
      }
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
  future: {
    purgeLayersByDefault: true,
    removeDeprecatedGapUtilities: true,
},


}
