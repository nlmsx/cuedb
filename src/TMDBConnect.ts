import { TMDB_API_KEY_CUEDB } from "../config";
import type { TMDBMovie } from "./types";

export default class TMDBConnect {
  private static API_KEY_TMDB = TMDB_API_KEY_CUEDB;
  private static BASE_URL = "https://api.themoviedb.org/3/";

  private static IMAGE_BASE_URL = "https://image.tmdb.org/t/p/original";

  private static async getEndpointJSON(endpoint: string): Promise<unknown> {
    const response = await fetch(
      this.BASE_URL + endpoint + `?api_key=${this.API_KEY_TMDB}`
    );
    return response.json();
  }

  public static async getMovie(id: string) {
    return (await this.getEndpointJSON("movie/" + id)) as Promise<TMDBMovie>;
  }

  public static getImageURL(imgRef: string) {
    return this.IMAGE_BASE_URL + imgRef;
  }
}
